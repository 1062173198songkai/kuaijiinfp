import com.google.gson.*;
import com.sun.xml.internal.bind.v2.runtime.InlineBinaryTransducer;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import sun.net.www.http.HttpClient;


import java.io.IOException;
import java.util.*;


public class AdressAccess {
    private static String queryUrl = "http://api.map.baidu.com/place/v2/search?query=%s&region=全国&output=json&scope=2&ak=rWRl8Z4FAIbAvcIm1ZRy5LwwDMHmTwac";
    private static String queryUrlCity = "http://api.map.baidu.com/place/v2/search?query=%s&region=%s&output=json&scope=2&ak=rWRl8Z4FAIbAvcIm1ZRy5LwwDMHmTwac";
    private static String rexTag = "（.*）";
    private static final String RESULT_TYPE = "result_type";
    private static  HashMap<String,String> errorMap = new HashMap<String, String>();
    private static HashMap<String,String> reslutMap = new HashMap<String,String>();
    public static class Poi{
        public String poiName;
        public Integer num;
        public  Poi(String poiName,Integer num){
            this.poiName = poiName;
            this.num = num;
        }
    }
    public static void main(String[] arg0){
        HashSet<String> poiSet = new HashSet<String>();
        HashMap<String,Integer> poiMap = new HashMap<String, Integer>();
        for(Map.Entry<String,String> entry : Process.map.entrySet()){
            poiSet.add(entry.getValue());
            Integer num = poiMap.get(entry.getValue());
            if(num == null){
                num = 0;
            }
            poiMap.put(entry.getValue(),++num);
        }
        ArrayList<Poi> poiList = new ArrayList<Poi>(AddressProcess.addressList.size());
        poiSet = null;
//        for(Map.Entry<String,Integer> entry : poiMap.entrySet()){
//            poiList.add(new Poi(entry.getKey(),entry.getValue()));
//        }
        for(String str : AddressProcess.addressList){
            poiList.add(new Poi(str,1));
        }
        Collections.sort(poiList, new Comparator<Poi>() {
            public int compare(Poi o1, Poi o2) {
                return o2.num-o1.num;
            }
        });
        System.out.println("index:0:"+poiList.get(0).num);
        poiMap = null;
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(100).setConnectTimeout(100).setSocketTimeout(100).build();
//        System.out.println(poiList.get(0).poiName.replaceAll(rexTag,""));
        for(Poi poi : poiList){
            String poiUrl = String.format(queryUrl,poi.poiName.replaceAll(" ",""));
            System.out.println(poi.poiName);
            System.out.println(poiUrl);
            try {
                HttpGet get = new HttpGet(poiUrl);
                HttpResponse httpResponse = httpClient.execute(get);
                String html = EntityUtils.toString(httpResponse.getEntity(),"UTF-8");
//                System.out.println(html);
                JsonParser parser = new JsonParser();
                JsonObject jsonObject = parser.parse(html).getAsJsonObject();
                String resultType = jsonObject.get(RESULT_TYPE).getAsString();
                switch (resultType){
                    case "poi_type":
                        JsonArray results = jsonObject.get("results").getAsJsonArray();
                        JsonObject result1 = results.get(0).getAsJsonObject();
                        String address = result1.get("address").getAsString();
                        reslutMap.put(poi.poiName,address);
                        break;
                    case "city_type":
                        JsonArray resultsCity = jsonObject.get("results").getAsJsonArray();
                        StringBuilder stringBuilder = new StringBuilder();
                        for(JsonElement jsonElement : resultsCity){
                            JsonObject jsonObjectCity = jsonElement.getAsJsonObject();
                            String cityName = jsonObjectCity.get("name").getAsString();
                            if(cityName.equals("吉林市")){
                                continue;
                            }
                            String poiCityUrl = String.format(queryUrlCity,poi.poiName.replaceAll(" ",""),cityName.replaceAll(" ",""));
                            HttpGet getCity = new HttpGet(poiCityUrl);
                            HttpResponse httpCityResponse = httpClient.execute(getCity);
                            String htmlCity = EntityUtils.toString(httpCityResponse.getEntity(),"UTF-8");
                            JsonParser parserCity = new JsonParser();
                            JsonObject jsonObjectCityRes = parserCity.parse(htmlCity).getAsJsonObject();
                            String resultTypeCity = jsonObjectCityRes.get(RESULT_TYPE).getAsString();
                            if(resultTypeCity.equals("poi_type")){
                                JsonArray resultsPoiCity = jsonObjectCityRes.get("results").getAsJsonArray();
                                JsonObject result1PoiCity = resultsPoiCity.get(0).getAsJsonObject();
                                String addressCity = result1PoiCity.get("address").getAsString();
                                try{
                                    String province = result1PoiCity.get("province").getAsString();
                                    stringBuilder.append(province);
                                }catch (Exception e){
                                }
                                try{
                                    String city = result1PoiCity.get("city").getAsString();
                                    stringBuilder.append(city);
                                }catch (Exception e){
                                }
                                try{
                                    String area = result1PoiCity.get("area").getAsString();
                                    stringBuilder.append(area);
                                }catch (Exception e){
                                }
                                stringBuilder.append(addressCity).append(",");
                            }
                        }
                        reslutMap.put(poi.poiName,stringBuilder.toString());
                        break;
                    default:
                        errorMap.put(poi.poiName,resultType);
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
            System.out.println("resultMap:");
            System.out.println(reslutMap);
            System.out.println("errorMap:");
            System.out.println(errorMap);
        }
    }
}
